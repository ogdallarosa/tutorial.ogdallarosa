#include "fatorial.h"

int fatorial(int x)
{
	if(x==1)
	{
		return 1;
	}
	else
	{
		if(x>1)
		{
			return (fatorial(x-1)*x);
		}
		else
			return -1;
	}
}